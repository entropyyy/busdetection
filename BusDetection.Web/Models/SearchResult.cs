﻿using System.Text.RegularExpressions;

namespace BusDetection.Web.Models
{
    public class SearchResult
    {
        public string BusSygnature { get; set; }
        public double Mean { get; set; }
        public bool Success { get; set; }
        private Match _result;
        public string RawText { get; }


        public SearchResult(string text, double mean)
        {
            this.RawText = text;
            this._result = Regex.Match(text, @"\d{3}");
            this.Success = _result.Success;
            this.Mean = mean;

            if (this.Success)
                this.BusSygnature = _result.Value;
        }

    }
}