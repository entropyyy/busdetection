﻿using System.Collections.Generic;
using DetectionEngine.Interfaces;

namespace DetectionEngine.Engine
{
    public class PredefDefaultImageProcessors
    {
        public IEnumerable<IImageProcessor> GetImageProcessors()
        {
            var imageProcessorList = new List<IImageProcessor>
            {
                new DefaultImageProcessor(0, 50, 1),
                new DefaultImageProcessor(0, 50, 2),
                new DefaultImageProcessor(0, 150, 1),
                new DefaultImageProcessor(0, 150, 2),
                new DefaultImageProcessor(0, 250, 1),
                new DefaultImageProcessor(0, 250, 2),
            };

            return imageProcessorList;
        }
    }
}
