﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Http;

namespace BusDetection.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var settigs = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;

            settigs.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settigs.Formatting = Formatting.Indented;

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


        }
    }
}
