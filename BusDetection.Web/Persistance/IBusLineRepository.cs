﻿using BusDetection.Web.Models;
using System.Collections.Generic;

namespace BusDetection.Web.Persistance
{
    public interface IBusLineRepository
    {
        IList<BusLine> GetAllBusses();
        BusLine GetBus(string syganture);
        void AddBus(BusLine bus);
        void DeleteBus(string sygnature);
    }
}
