﻿using System.Drawing;
using DetectionEngine.Interfaces;
using Emgu.CV;
using Emgu.CV.CvEnum;

namespace DetectionEngine.Engine
{
    public class DefaultImageProcessor : IImageProcessor
    {
        private readonly int _lowerTresh;
        private readonly int _upperTresh;
        private readonly int _gauss;

        public DefaultImageProcessor(int lowerTresh, int upperTresh, int gauss)
        {
            _lowerTresh = lowerTresh;
            _upperTresh = upperTresh;
            _gauss = gauss;
        }

        public void ProcessImage(IImage image)
        {
            CvInvoke.CvtColor(image, image, ColorConversion.Bgr2Gray);
            CvInvoke.GaussianBlur(image, image, new Size(), _gauss);
            CvInvoke.Canny(image, image, _lowerTresh, _upperTresh);
        }
    }
}
