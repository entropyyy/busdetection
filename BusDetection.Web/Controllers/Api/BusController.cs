﻿using BusDetection.Web.Models;
using BusDetection.Web.Persistance;
using DetectionEngine.Engine;
using DetectionEngine.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.Http;
using Tesseract;

namespace BusDetection.Web.Controllers.Api
{
    [System.Web.Http.RoutePrefix("api/bus")]
    public class BusController : ApiController
    {
        private readonly IBusLineRepository _busRepository;
        private readonly IImageProcessor _defaultImageProcessor;
        private readonly IBoxProcessor _defaultBoxProcessor;
        private readonly ImageLoader _imageLoader;

        public BusController()
        {

            _imageLoader = new ImageLoader();
            _busRepository = new BusLineRepository();
        }

        [Route("test")]
        public IHttpActionResult PostTest([FromBody] string test)
        {
            try
            {
                if (test == "test")
                    return Ok("udało się");
                else
                    return Ok("nie udało się");
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        [Route("processImage")]
        public IHttpActionResult PostUploadImage()
        {
            List<SearchResult> results = new List<SearchResult>();
            List<BusLine> buses = new List<BusLine>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile == null || postedFile.ContentLength <= 0) continue;

                    var img = _imageLoader.LoadFromBitmap(new Bitmap(postedFile.InputStream));
                    var predefImageProcessors = new PredefDefaultImageProcessors();
                    var detectionEngine = new BoxDetectionEngine(predefImageProcessors.GetImageProcessors(), new DefaultBoxProcessor());
                    var boxes = detectionEngine.GetMatchingBoxes(img);

                    foreach (Bitmap bitmap in boxes)
                        using (var engine = new TesseractEngine(HttpContext.Current.Server.MapPath(@"~/tessdata"), "eng", EngineMode.Default))
                        using (var image = new Bitmap(bitmap))
                        using (var pix = PixConverter.ToPix(image))
                        using (var page = engine.Process(pix))
                        {
                            var result = new SearchResult(page.GetText(), page.GetMeanConfidence());
                            if (!result.Success) continue;

                            var bus = _busRepository.GetBus(result.BusSygnature);
                            if (bus == null) continue;

                            buses.Add(bus);
                            break;
                        }
                }

                return Ok(_busRepository.GetBus("119"));
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }

        [Route("testProcessImage")]
        public IHttpActionResult PostTestUploadImage()
        {
            List<SearchResult> results = new List<SearchResult>();
            List<BusLine> buses = new List<BusLine>();
            try
            {
                //var httpRequest = HttpContext.Current.Request;

                //foreach (string file in httpRequest.Files)
                //{
                //    var postedFile = httpRequest.Files[file];
                //    if (postedFile == null || postedFile.ContentLength <= 0) continue;

                //    Bitmap bmp = new Bitmap(postedFile.InputStream);

                //    IImage img = new Image<Bgr, Byte>(bmp);

                //    var detectionEngine = new BoxDetectionEngine(_defaultImageProcessor, _defaultBoxProcessor);
                //    var boxes = detectionEngine.GetMatchingBoxes(img);

                //    foreach (Bitmap bitmap in boxes)
                //        using (var engine = new TesseractEngine(@"h:/root/home/dariuszdbr-001/www/site/tessdata", "eng", EngineMode.Default))
                //        using (var image = new Bitmap(bitmap))
                //        using (var pix = PixConverter.ToPix(image))
                //        using (var page = engine.Process(pix))
                //        {
                //            var result = new SearchResult(page.GetText(), page.GetMeanConfidence());
                //            if (!result.Success) continue;

                //            results.Add(result);
                //            buses.Add(_busRepository.GetBus(result.BusSygnature));
                //        }
                //}


                return Ok(_busRepository.GetBus("119"));
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }
    }
}
