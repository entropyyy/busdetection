﻿using System;
using System.Drawing;
using System.IO;
using DetectionEngine.Interfaces;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace DetectionEngine.Engine
{
    public class ImageLoader : IImageLoader
    {
        public IImage LoadFromFile(string path)
        {
            try
            {
                if (!File.Exists(path))
                    throw new FileNotFoundException();
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono obrazkow do wczytania");
                Console.ReadKey();
                Environment.Exit(-1);
            }

            return CvInvoke.Imread(path, ImreadModes.AnyColor);
        }

        public IImage LoadFromBitmap(Bitmap bitmap)
        {
            if (bitmap == null) throw new ArgumentNullException();

            var image = new Image<Bgr,Byte>(bitmap);
            return image;
        }
    }
}
