﻿using System.Web;

namespace BusDetection.Web.ViewModels
{
    public class UploadImageViewModel
    {
        public HttpPostedFileBase File { get; set; }
    }
}
