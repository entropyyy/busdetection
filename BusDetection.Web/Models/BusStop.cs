﻿namespace BusDetection.Web.Models
{
    public class BusStop
    {
        public string Name { get; set; }

        public BusStop(string name)
        {
            Name = name;

        }
    }
}